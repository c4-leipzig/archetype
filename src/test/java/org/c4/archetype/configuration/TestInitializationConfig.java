package org.c4.archetype.configuration;

import org.c4.archetype.domainevent.DomainEventHandlingContext;
import org.c4.archetype.domainevent.repository.DomainEventRepository;
import org.c4.archetype.initialization.InitializationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class TestInitializationConfig
{
    @Bean
    @Profile("test")
    InitializationRunner initializationRunner(DomainEventRepository domainEventRepository,
            DomainEventHandlingContext initialDomainEventHandlingContextImpl)
    {
        return new InitializationRunner(domainEventRepository, initialDomainEventHandlingContextImpl);
    }
}

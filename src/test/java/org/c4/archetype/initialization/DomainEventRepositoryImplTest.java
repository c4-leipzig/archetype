package org.c4.archetype.initialization;

import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.bson.types.ObjectId;
import org.c4.archetype.communication.StringListDto;
import org.c4.archetype.domainevent.communication.DomainEvent;
import org.c4.archetype.domainevent.repository.DomainEventRepository;
import org.c4.archetype.test.TestEvent;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClientException;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class DomainEventRepositoryImplTest
{
    @Autowired
    private DomainEventRepository domainEventRepository;

    @Autowired
    ObjectMapper objectMapper;

    private MockWebServer mockWebServer;

    @BeforeEach
    void setUp() throws IOException
    {
        mockWebServer = new MockWebServer();
        mockWebServer.start(65001);
    }

    @AfterEach
    void tearDown() throws IOException
    {
        mockWebServer.shutdown();
    }

    /**
     * Testet, dass die korrekten Requests an den Eventstore gemacht werden.
     */
    @Test
    void getRelevantEvents() throws Exception
    {
        List<String> aggregateIds = Arrays.asList(new ObjectId().toHexString(), new ObjectId().toHexString());
        DomainEvent d1 = createDomainEvent(aggregateIds.get(0));
        DomainEvent d2 = createDomainEvent(aggregateIds.get(1));

        mockWebServer.enqueue(
                new MockResponse().setBody(objectMapper.writeValueAsString(new StringListDto(aggregateIds)))
                        .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON));
        mockWebServer.enqueue(new MockResponse().setBody(objectMapper.writeValueAsString(d1))
                .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON));
        mockWebServer.enqueue(new MockResponse().setBody(objectMapper.writeValueAsString(d2))
                .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON));

        List<List<DomainEvent>> resultList = domainEventRepository.getRelevantEvents(
                Collections.singletonList("TestEvent"));

        assertNotNull(resultList);
        assertFalse(resultList.isEmpty());
        assertThat(resultList.size(), is(equalTo(2)));
        List<String> ids = resultList.stream()
                .map(domainEvents -> domainEvents.get(0).getAggregateId())
                .collect(Collectors.toList());
        assertThat(ids, contains(aggregateIds.get(0), aggregateIds.get(1)));
        assertThat(mockWebServer.getRequestCount(), is(equalTo(3)));
    }

    /**
     * Wenn der Eventstore nicht erreichbar ist, wird eine Exception geworfen.
     */
    @Test
    void getRelevantEvents_errorThrown() throws Exception
    {
        mockWebServer.enqueue(new MockResponse().setResponseCode(400));

        assertThrows(WebClientException.class,
                () -> domainEventRepository.getRelevantEvents(Collections.singletonList("TestEvent")));
    }

    /**
     * EventStream wird korrekt zurückgegeben.
     */
    @Test
    void getEventStream() throws Exception
    {
        DomainEvent d1 = createDomainEvent(new ObjectId().toHexString());
        DomainEvent d2 = createDomainEvent(d1.getAggregateId());

        mockWebServer.enqueue(
                new MockResponse().setBody(objectMapper.writeValueAsString(Arrays.asList(d1, d2)))
                        .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON));

        List<DomainEvent> resultList = domainEventRepository.getEventStream(d1.getAggregateId());

        assertNotNull(resultList);
        assertFalse(resultList.isEmpty());
        assertThat(resultList.size(), is(equalTo(2)));
        assertThat(mockWebServer.getRequestCount(), is(equalTo(1)));
    }

    private DomainEvent createDomainEvent(String id)
    {
        DomainEvent domainEvent = new TestEvent();
        domainEvent.setAggregateId(id);

        return domainEvent;
    }
}
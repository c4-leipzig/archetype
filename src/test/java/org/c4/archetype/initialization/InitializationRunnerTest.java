package org.c4.archetype.initialization;

import org.c4.archetype.domainevent.communication.DomainEvent;
import org.c4.archetype.domainevent.DomainEventHandlingContext;
import org.c4.archetype.domainevent.repository.DomainEventRepository;
import org.c4.archetype.test.TestEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
class InitializationRunnerTest
{
    @Mock
    DomainEventRepository domainEventRepository;

    @Mock
    DomainEventHandlingContext domainEventHandlingContext;

    private InitializationRunner initializationRunner;

    @BeforeEach
    void setUp()
        {
        initializationRunner = new InitializationRunner(domainEventRepository,
                domainEventHandlingContext);
    }

    /**
     * Der HandlingContext wird korrekt aufgerufen.
     */
    @Test
    void handleEventGetsCalled()
    {
        List<DomainEvent> domainEvents = createEvents(5);
        doReturn(Collections.singletonList(domainEvents)).when(domainEventRepository).getRelevantEvents(any());

        initializationRunner.run();

        verify(domainEventHandlingContext, times(5)).handleEvent(any());
    }

    private List<DomainEvent> createEvents(int amount)
    {
        List<DomainEvent> domainEvents = new ArrayList<>();
        for (int i = amount; i > 0; i--)
        {
            TestEvent testEvent = new TestEvent();
            testEvent.setVersion(1L);
            domainEvents.add(testEvent);
        }
        return domainEvents;
    }
}

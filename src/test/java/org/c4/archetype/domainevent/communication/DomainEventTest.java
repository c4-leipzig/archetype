package org.c4.archetype.domainevent.communication;

import org.bson.types.ObjectId;
import org.c4.archetype.domainevent.DomainEventHandlingContext;
import org.c4.archetype.domainevent.DomainEventProcessingState;
import org.c4.archetype.test.TestEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mock;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class DomainEventTest
{
    @Mock
    DomainEventHandlingContext handlingContext;

    @BeforeEach
    void setUp()
    {
        initMocks(this);
    }

    /**
     * Ausgehend von CurrentVersion und version soll der processingState entsprechend gesetzt werden.
     * Zusätzlich wird geprüft, dass die handleEvents-Methode korrekt aufgerufen wird.
     */
    @ParameterizedTest(name = "CurrentVersion: {0}, version: {1} should return processingState {2} and call handleEvents {3} times")
    @CsvSource({ ", 0, PROCESSED, 1", ", 1, VERSION_MISMATCH, 0", "2, 1, OBSOLETE, 0", "1, 2, PROCESSED, 1",
            "1, 3, VERSION_MISMATCH, 0" })
    void handleEvent(Long currentVersion, long version, String processingState, int handleEventCalls)
    {
        DomainEvent domainEvent = createDomainEvent();
        domainEvent.setVersion(version);
        doReturn(currentVersion).when(handlingContext).getVersion(any());

        DomainEventProcessingState result = domainEvent.handleEvent(handlingContext);

        assertThat(result, is(equalTo(DomainEventProcessingState.valueOf(processingState))));
        verify(handlingContext, times(handleEventCalls)).handleEvent(any());
    }

    private DomainEvent createDomainEvent()
    {
        DomainEvent domainEvent = new TestEvent();
        domainEvent.setAggregateId(new ObjectId().toHexString());
        return domainEvent;
    }
}
package org.c4.archetype.domainevent;

import org.c4.archetype.domainevent.communication.DomainEvent;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

class DomainEventSinkTest
{
    private DomainEventSink domainEventSink;

    private DomainEventHandlingContext handlingContext;

    @Test
    void consumeDomainEvent()
    {
        DomainEvent domainEvent = mock(DomainEvent.class);
        handlingContext = mock(DomainEventHandlingContext.class);
        domainEventSink = new DomainEventSink(handlingContext);

        domainEventSink.consumeDomainEvent(domainEvent);

        verify(domainEvent, times(1)).apply(handlingContext);
    }
}
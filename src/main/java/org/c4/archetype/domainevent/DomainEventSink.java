package org.c4.archetype.domainevent;

import org.c4.archetype.domainevent.communication.DomainEvent;
import org.springframework.kafka.annotation.KafkaListener;

/**
 * KafkaListener für alle {@link DomainEvent}s.
 * <br/>
 * Copyright: Copyright (c) 17.12.2019 <br/>
 * Organisation: Exxeta GmbH
 *
 * @author Jan Buchholz <a href="mailto:jan.buchholz@exxeta.com">jan.buchholz@exxeta.com</a>
 */
public class DomainEventSink
{
    private final DomainEventHandlingContext domainEventHandlingContext;

    public DomainEventSink(DomainEventHandlingContext domainEventHandlingContext)
    {
        this.domainEventHandlingContext = domainEventHandlingContext;
    }

    @KafkaListener(topics = "domainEventFromEventStoreTopic")
    public void consumeDomainEvent (DomainEvent domainEvent) {
        domainEvent.apply(domainEventHandlingContext);
    }
}

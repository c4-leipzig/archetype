package org.c4.archetype.domainevent;

/**
 * Mögliche Zustände, die bei der Verarbeitung eines DomainEvents auftreten können.
 * <br/>
 * Copyright: Copyright (c) 07.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public enum DomainEventProcessingState
{
    /**
     * Normaler Zustand
     */
    PROCESSED,
    /**
     * Event ist älter als der lokale Zustand
     */
    OBSOLETE,
    /**
     * Event ist neuer als der lokale Zustand, d.h. es fehlen Events
     */
    VERSION_MISMATCH
}

package org.c4.archetype.domainevent;

import org.c4.archetype.test.TestEvent;

/**
 * Context zur Verarbeitung eingehender Events.
 * <br/>
 * Copyright: Copyright (c) 03.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public interface DomainEventHandlingContext
{
    /**
     * Funktion zur Verarbeitung von {@link TestEvent}s.
     */
    void handleEvent(TestEvent testEvent);

    Long getVersion(TestEvent testEvent);
}

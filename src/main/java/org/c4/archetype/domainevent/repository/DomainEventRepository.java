package org.c4.archetype.domainevent.repository;

import org.c4.archetype.domainevent.communication.DomainEvent;

import java.util.List;

/**
 * Repository für den Zugriff auf DomainEvents vom Eventstore.
 * <br/>
 * Copyright: Copyright (c) 04.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public interface DomainEventRepository
{
    /**
     * Liefert - ausgehend von einer Liste von Eventtypen - alle entsprechenden EventStreams.
     */
    List<List<DomainEvent>> getRelevantEvents(List<String> eventTypes);

    /**
     * Liefert den EventStream für ein Aggregate.
     */
    List<DomainEvent> getEventStream(String aggregateId);
}

package org.c4.archetype.domainevent.repository;

import org.c4.archetype.configuration.properties.EventstoreProperties;
import org.c4.archetype.domainevent.communication.DomainEvent;
import org.c4.archetype.communication.StringListDto;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * WebClient zur Abfrage von DomainEvents im Zuge der initialen Aufdatung.
 * <br/>
 * Copyright: Copyright (c) 03.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public class DomainEventRepositoryImpl implements DomainEventRepository
{
    private final WebClient webClient;

    private final EventstoreProperties eventstoreProperties;

    public DomainEventRepositoryImpl(WebClient webClient, EventstoreProperties eventstoreProperties)
    {
        this.webClient = webClient;
        this.eventstoreProperties = eventstoreProperties;
    }

    @Override
    public List<List<DomainEvent>> getRelevantEvents(List<String> eventTypes)
    {
        List<String> aggregateIds = webClient.get()
                .uri(uriBuilder -> uriBuilder.path(eventstoreProperties.getAggregateIdsByEventTypes())
                        .queryParam("eventTypes", String.join(",", eventTypes))
                        .build())
                .retrieve()
                .bodyToMono(StringListDto.class)
                .map(StringListDto::getStringList)
                .block();

        if (aggregateIds == null)
        {
            return Collections.emptyList();
        }

        List<List<DomainEvent>> result = new ArrayList<>();

        aggregateIds.forEach(aggregateId -> result.add(getEventStream(aggregateId)));

        return result;
    }

    @Override
    public List<DomainEvent> getEventStream(String aggregateId)
    {
        return webClient.get()
                .uri(eventstoreProperties.getEventStreamForAggregate(), aggregateId)
                .retrieve()
                .bodyToFlux(DomainEvent.class)
                .collectList()
                .block();
    }
}

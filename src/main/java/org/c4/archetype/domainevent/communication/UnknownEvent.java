package org.c4.archetype.domainevent.communication;

import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.c4.archetype.domainevent.DomainEventHandlingContext;

/**
 * Sammelstelle für alle Events, die nichts mit diesem Service zu tun haben.
 * <br/>
 * Copyright: Copyright (c) 03.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
@Setter
public class UnknownEvent extends DomainEvent
{
    private String classIdentifier;

    @Override
    public void apply(DomainEventHandlingContext handlingContext)
    {
        log.debug("Unknown event of type '{}' discarded", classIdentifier);
    }

    @Override
    protected Long getCurrentVersion(DomainEventHandlingContext handlingContext)
    {
        return null;
    }

    @Override
    protected boolean isSnapshotEvent()
    {
        return false;
    }
}

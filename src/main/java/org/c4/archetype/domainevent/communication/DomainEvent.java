package org.c4.archetype.domainevent.communication;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.Setter;
import org.c4.archetype.domainevent.DomainEventHandlingContext;
import org.c4.archetype.domainevent.DomainEventProcessingState;
import org.c4.archetype.test.TestEvent;

import java.time.Instant;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, defaultImpl = UnknownEvent.class, property = "classIdentifier")
@JsonSubTypes({ @JsonSubTypes.Type(value = TestEvent.class, name = "TestEvent") })
@Getter
@Setter
public abstract class DomainEvent
{
    // Wird immer vom erstellenden Service gesetzt
    protected String aggregateId;
    protected String createdBy;

    // Wird vom Eventstore beim tatsächlichen Einfügen gesetzt
    protected Instant createdAt;

    // Wird vom Eventstore vorgegeben
    protected Long version;

    public DomainEventProcessingState handleEvent(DomainEventHandlingContext handlingContext)
    {
        Long currentVersion = getCurrentVersion(handlingContext);
        boolean isSnapshot = isSnapshotEvent();

        DomainEventProcessingState processingState;

        if (currentVersion == null)
        {
            if (isSnapshot || version == 0L)
            {
                processingState = DomainEventProcessingState.PROCESSED;
            }
            else
            {
                processingState = DomainEventProcessingState.VERSION_MISMATCH;
            }
        }
        else if (version <= currentVersion)
        {
            processingState = DomainEventProcessingState.OBSOLETE;
        }
        else if ((version == currentVersion + 1) || isSnapshot)
        {
            processingState = DomainEventProcessingState.PROCESSED;
        }
        else
        {
            processingState = DomainEventProcessingState.VERSION_MISMATCH;
        }

        if (processingState == DomainEventProcessingState.PROCESSED)
        {
            apply(handlingContext);
        }

        return processingState;
    }

    /**
     * Methode die ausgeführt wird, um das Event zu verarbeiten.
     */
    public abstract void apply(DomainEventHandlingContext handlingContext);

    /**
     * Liefert die aktuelle Version des zugehörigen Aggregats.
     */
    protected abstract Long getCurrentVersion(DomainEventHandlingContext handlingContext);

    /**
     * Jede Subclass gibt an, ob sie ein SnapshotEvent ist oder nicht.
     */
    protected abstract boolean isSnapshotEvent();
}

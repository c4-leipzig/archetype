package org.c4.archetype.domainevent;

import lombok.extern.log4j.Log4j2;
import org.c4.archetype.test.TestEvent;

/**
 * Implementation zur Verarbeitung von Events, die zur Laufzeit eingehen.
 * <br/>
 * Copyright: Copyright (c) 03.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
public class OngoingDomainEventHandlingContextImpl implements DomainEventHandlingContext
{
    @Override
    public void handleEvent(TestEvent testEvent)
    {
        log.debug("Event 'TestEvent' in processing");
    }

    @Override
    public Long getVersion(TestEvent testEvent)
    {
        return 0L;
    }
}

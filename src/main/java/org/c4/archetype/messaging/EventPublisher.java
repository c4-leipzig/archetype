package org.c4.archetype.messaging;

import org.c4.archetype.domainevent.communication.DomainEvent;

/**
 * Publisher für Events.
 * <br/>
 * Copyright: Copyright (c) 22.12.2019 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public interface EventPublisher
{
    /**
     * Sendet ein DomainEvent. Die Topic muss in der jeweiligen Implementierung definiert werden.
     */
    void send(DomainEvent domainEvent);
}

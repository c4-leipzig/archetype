package org.c4.archetype.communication;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Utility-Class für das Versenden von String-Listen.
 * <br/>
 * Copyright: Copyright (c) 05.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StringListDto
{
    private List<String> stringList;
}

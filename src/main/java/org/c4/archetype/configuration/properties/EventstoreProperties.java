package org.c4.archetype.configuration.properties;

import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Properties für die Verbindung zum Eventstore.
 * <br/>
 * Copyright: Copyright (c) 03.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Configuration
@ConfigurationProperties(prefix = "c4.eventstore")
@Setter
public class EventstoreProperties
{
    private String baseUrl;
    private String aggregateIdsByEventTypes;
    private String eventStreamForAggregate;
    private String eventListForAggregate;

    public String getAggregateIdsByEventTypes()
    {
        return baseUrl + aggregateIdsByEventTypes;
    }

    public String getEventStreamForAggregate()
    {
        return baseUrl + eventStreamForAggregate;
    }

    public String getEventListForAggregate()
    {
        return baseUrl + eventListForAggregate;
    }
}

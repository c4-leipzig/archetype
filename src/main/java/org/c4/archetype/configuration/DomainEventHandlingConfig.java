package org.c4.archetype.configuration;

import org.c4.archetype.configuration.properties.EventstoreProperties;
import org.c4.archetype.domainevent.DomainEventHandlingContext;
import org.c4.archetype.domainevent.DomainEventSink;
import org.c4.archetype.domainevent.OngoingDomainEventHandlingContextImpl;
import org.c4.archetype.domainevent.repository.DomainEventRepository;
import org.c4.archetype.domainevent.repository.DomainEventRepositoryImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * Config für das Verarbeiten von DomainEvents.
 * <br/>
 * Copyright: Copyright (c) 03.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Configuration
public class DomainEventHandlingConfig
{
    @Bean
    public DomainEventHandlingContext ongoingDomainEventHandlingContextImpl()
    {
        return new OngoingDomainEventHandlingContextImpl();
    }

    // Die DomainEventSink darf erst erstellt werden, wenn die Initialisierung abgeschlossen ist.
    @Bean
    @DependsOn(value = { "initializationRunner" })
    public DomainEventSink domainEventSink(DomainEventHandlingContext ongoingDomainEventHandlingContextImpl)
    {
        return new DomainEventSink(ongoingDomainEventHandlingContextImpl);
    }

    @Bean
    public DomainEventRepository domainEventRepository(WebClient webClient,
            EventstoreProperties eventstoreProperties)
    {
        return new DomainEventRepositoryImpl(webClient, eventstoreProperties);
    }
}

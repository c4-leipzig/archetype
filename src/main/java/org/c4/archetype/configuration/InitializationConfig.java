package org.c4.archetype.configuration;

import org.c4.archetype.domainevent.DomainEventHandlingContext;
import org.c4.archetype.domainevent.repository.DomainEventRepository;
import org.c4.archetype.initialization.InitialDomainEventHandlingContextImpl;
import org.c4.archetype.initialization.InitializationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * Config für die Initalisierung des Services.
 * <br/>
 * Copyright: Copyright (c) 06.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Configuration
public class InitializationConfig
{
    @Bean(initMethod = "run")
    @Profile("!test")
    InitializationRunner initializationRunner(DomainEventRepository domainEventRepository,
            DomainEventHandlingContext initialDomainEventHandlingContextImpl)
    {
        return new InitializationRunner(domainEventRepository, initialDomainEventHandlingContextImpl);
    }

    @Bean
    DomainEventHandlingContext initialDomainEventHandlingContextImpl()
    {
        return new InitialDomainEventHandlingContextImpl();
    }
}

package org.c4.archetype.initialization;

import lombok.extern.log4j.Log4j2;
import org.c4.archetype.domainevent.DomainEventHandlingContext;
import org.c4.archetype.test.TestEvent;

/**
 * Implementation zur Verarbeitung von Events, die im Rahmen der initialen Aufdatung anfallen.
 * <br/>
 * Copyright: Copyright (c) 03.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
public class InitialDomainEventHandlingContextImpl implements DomainEventHandlingContext
{
    @Override
    public void handleEvent(TestEvent testEvent)
    {
        log.debug("Event 'TestEvent' in processing");
    }

    @Override
    public Long getVersion(TestEvent testEvent)
    {
        return 0L;
    }
}

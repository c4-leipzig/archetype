package org.c4.archetype.initialization;

import lombok.extern.log4j.Log4j2;
import org.c4.archetype.domainevent.communication.DomainEvent;
import org.c4.archetype.domainevent.DomainEventHandlingContext;
import org.c4.archetype.domainevent.DomainEventProcessingState;
import org.c4.archetype.domainevent.repository.DomainEventRepository;

import java.util.Collections;
import java.util.List;

/**
 * Bean zur initialen Aufdatung des Services. Die Operationen sind bewusst Blocking und der KafkaListener wartet,
 * bis dieser Bean fertig ist. Damit wird sichergestellt, dass der Service erst nach seiner Aufdatung
 * mit der Verarbeitung anfängt.
 * <br/>
 * Copyright: Copyright (c) 06.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
public class InitializationRunner
{
    private static final List<String> RELEVANT_EVENTS = Collections.singletonList("TestEvent");

    private final DomainEventRepository domainEventRepository;

    private final DomainEventHandlingContext handlingContext;

    public InitializationRunner(DomainEventRepository domainEventRepository,
            DomainEventHandlingContext handlingContext)
    {
        this.domainEventRepository = domainEventRepository;
        this.handlingContext = handlingContext;
    }

    public void run()
    {
        domainEventRepository.getRelevantEvents(RELEVANT_EVENTS).forEach(this::handleEvents);
    }

    private void reloadAggregate(String aggregateId)
    {
        List<DomainEvent> domainEvents = domainEventRepository.getEventStream(aggregateId);
        handleEvents(domainEvents);
    }

    private void handleEvents(List<DomainEvent> domainEvents)
    {
        if (domainEvents == null || domainEvents.isEmpty())
        {
            return;
        }

        log.debug("Processing aggregate '{}'", domainEvents.get(0).getAggregateId());
        for (DomainEvent domainEvent : domainEvents)
        {
            DomainEventProcessingState processingState = domainEvent.handleEvent(handlingContext);
            if (processingState == DomainEventProcessingState.VERSION_MISMATCH)
            {
                log.info("Version mismatch for aggregate '{}'. Version {} does not match.",
                        domainEvent.getAggregateId(), domainEvent.getVersion());
                reloadAggregate(domainEvent.getAggregateId());
                return;
            }
        }
    }
}

package org.c4.archetype.test;

import lombok.Getter;
import lombok.Setter;
import org.c4.archetype.domainevent.communication.DomainEvent;
import org.c4.archetype.domainevent.DomainEventHandlingContext;

/**
 * Testevent zur Demonstration.
 * <br/>
 * Copyright: Copyright (c) 03.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@Setter
public class TestEvent extends DomainEvent
{
    private String testProperty;

    @Override
    public void apply(DomainEventHandlingContext handlingContext)
    {
        handlingContext.handleEvent(this);
    }

    @Override
    protected Long getCurrentVersion(DomainEventHandlingContext handlingContext)
    {
        return handlingContext.getVersion(this);
    }

    @Override
    protected boolean isSnapshotEvent()
    {
        return false;
    }
}

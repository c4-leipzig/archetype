package org.c4.archetype.test;

import org.c4.archetype.domainevent.communication.DomainEvent;
import org.c4.archetype.domainevent.repository.DomainEventRepository;
import org.c4.archetype.messaging.EventPublisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.Collections;
import java.util.List;

@RestController
public class TestController
{
    @Autowired
    EventPublisher eventPublisher;

    @Autowired
    Jackson2ObjectMapperBuilder objectMapperBuilder;

    @Autowired
    private DomainEventRepository domainEventRepository;

    @GetMapping("/test")
    public ResponseEntity<?> test(Principal principal) throws InterruptedException
    {
        TestEvent event = new TestEvent();
        event.setAggregateId("5dfa1fc28a953821d62b5be5");

        List<List<DomainEvent>> list = domainEventRepository.getRelevantEvents(Collections.singletonList("TestEvent"));


        return ResponseEntity.ok(null);
    }
}
